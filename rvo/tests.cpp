#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

vector<int> create_vector_rvo(size_t size)
{
    return vector<int>(size); // return of prvalue
}

vector<int> create_vector_nrvo(size_t size)
{
    vector<int> vec;

    vec.reserve(size);

    for(size_t i = 0; i < size; ++i)
        vec.push_back(i);

    return vec; // return of lvalue
}

TEST_CASE("rvo")
{
    vector<int> data = create_vector_rvo(1'000'000);

    auto dataset = create_vector_nrvo(2'000);
}

struct CopyMoveDisabled
{
    vector<int> data;

    CopyMoveDisabled(const CopyMoveDisabled&) = delete;
    CopyMoveDisabled& operator=(const CopyMoveDisabled&) = delete;
    CopyMoveDisabled(CopyMoveDisabled&&) = delete;
    CopyMoveDisabled& operator=(CopyMoveDisabled&&) = delete;
};

CopyMoveDisabled create_cmd(size_t size = 3)
{
    if (size == 13)
        throw std::runtime_error("I don't like 13");

    if (size == 3)
        return CopyMoveDisabled{ {42, 665, 13} };

    return CopyMoveDisabled{ vector(size, 1)};
}

// CopyMoveDisabled create_cmd_nrvo()
// {
//     CopyMoveDisabled cmd { {42, 665, 13} };
//     return cmd;
// }

void use(CopyMoveDisabled cmd)
{   
    cout << "Using cmd: ";
    for(const auto& item : cmd.data)
        cout << item << " ";
    cout << "\n";
}

TEST_CASE("returning or passing CopyMoveDisabled")
{
    CopyMoveDisabled cmd1{ {1, 2, 3} };
    //auto cmd2 = move(cmd1);

    CopyMoveDisabled cmd2 = create_cmd();

    for(const auto& item : cmd2.data)
        cout << item << " ";
    cout << "\n";

    //CopyMoveDisabled cmd3 = create_cmd_nrvo(); // still error

    use(create_cmd(10));
}