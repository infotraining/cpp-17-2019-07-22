#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct Gadget
{
    string id;
    int usage_counter = 0;

    explicit Gadget(string id)
        : id {move(id)}
    {
        cout << "Gadget(" << this->id << ": " << this << ")\n";
    }

    Gadget(const Gadget& source)
        : id {source.id}
    {
        cout << "Gadget(cc: " << this->id << ": " << this << ")\n";
    }

    Gadget(Gadget&& source) noexcept
        : id {move(source.id)}
    {
        cout << "Gadget(mv: " << this->id << ": " << this << ")\n";
    }

    ~Gadget()
    {
        cout << "~Gadget(" << this->id << ": " << this << ")\n";
    }

    void use()
    {
        ++usage_counter;
    }

    auto report()
    {
        return [*this] { cout << "Report from Gadget(" << id << ")\n"; };
    }
};


TEST_CASE("noexcept")
{
    vector<Gadget> gadgets;

    for(int i = 1; i <= 32; ++i)
    {
        gadgets.push_back(Gadget{to_string(i)});
        cout << "--------------------\n";
    }
}

void foo1()
{
    cout << "may throw foo()\n";
}

void foo2() noexcept
{
    cout << "noexcept foo()\n";
}

TEST_CASE("noexcept in C++17")
{
    static_assert(is_same_v<decltype(foo1), decltype(foo2)>);

    void (*ptr_fun_safe)() noexcept;

    ptr_fun_safe = &foo2;
    // ptr_fun_safe = &foo1; // since C++17 illegal

    void (*ptr_fun_unsafe)();
    ptr_fun_unsafe = &foo1;
    ptr_fun_unsafe = &foo2;
}