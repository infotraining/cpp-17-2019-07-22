#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct Result
{
    string msg;
    int err_code;

    ~Result()
    {
        cout << "~Result(" << msg << ", " << err_code << ")\n";
    }
};

[[nodiscard]] Result save_file(const string& filename)
{
    [[maybe_unused]] int status_code = 42;

    if (filename == "_")
        return Result{ "bad filename", 13 };

    return Result{"OK", 0};
}

namespace [[deprecated]] LegacyCode
{
    void foo()
    {}
}

TEST_CASE("nodiscard")
{
    if (auto [msg, err] = save_file("_"); err != 0)
    {
        cout << "Error: " << msg << "\n";
    }
    else
    {
        cout << msg << "\n";
    }
}

TEST_CASE("decprecated")
{
    LegacyCode::foo();
}