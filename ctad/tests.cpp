#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <mutex>

#include "catch.hpp"

using namespace std;

void foo(int)
{}

template <typename T>
void deduce1(T arg)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void deduce2(T& arg)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void deduce3(T&& arg)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("Template Argument Deduction - Case 1")
{
    int x = 10;
    const int cx = 10;
    int& ref_x = x;
    const int& cref_x = x;
    int tab[10];

    deduce1(x);
    auto ax1 = x; // int

    deduce1(cx);
    auto ax2 = cx; // int
    
    deduce1(ref_x);
    auto ax3 = ref_x; // int

    deduce1(cref_x);
    auto ax4 = cref_x; // int

    deduce1(tab);
    auto ax5 = tab; // int*

    deduce1(foo);
    auto ax6 = foo; // void(*)(int)
}

TEST_CASE("Template Argument Deduction - Case 2")
{
    cout << "\n-----------\n";
    int x = 10;
    const int cx = 10;
    int& ref_x = x;
    const int& cref_x = x;
    int tab[10];

    deduce2(x);
    auto& ax1 = x; // int

    deduce2(cx);
    auto& ax2 = cx; // const int
    
    deduce2(ref_x);
    auto& ax3 = ref_x; // int&

    deduce2(cref_x);
    auto& ax4 = cref_x; // const int&

    deduce2(tab);
    auto& ax5 = tab; // int(&)[10] - reference to table

    deduce2(foo);
    auto& ax6 = foo; // void(&)(int) - reference to function
}

TEST_CASE("Template Argument Deduction - Case 3")
{
    cout << "\n-----------\n";

    int x;

    deduce3(x);
    auto&& ux1 = x; // int&

    deduce3(42);
    auto&& ux2 = 42; // int&&
}

template <typename T1, typename T2>
struct ValuePair
{
    T1 fst;
    T2 snd;
    
    ValuePair(const T1& arg1, const T2& arg2) : fst{arg1}, snd{arg2}
    {}

    template <typename T>
    ValuePair(T a) : fst{a}, snd{a}
    {}
};

TEST_CASE("aaa")
{
    string s = "text";
    vector v = {1, 2, 3};

    ValuePair vp{s, v};
}

// deduction guide
template <typename T1, typename T2>
ValuePair(T1, T2) -> ValuePair<T1, T2>;

ValuePair(const char*, const char*) -> ValuePair<string, string>;

template <typename T>
ValuePair(T) -> ValuePair<T, T>;


TEST_CASE("Class Template Argument Deduction")
{
    ValuePair<int, double> vp1{1, 3.14}; 

    ValuePair vp2{1, 3.14}; // ValuePair<int, double>

    const int x = 10;

    ValuePair vp3{x, "text"};
    static_assert(is_same_v<decltype(vp3), ValuePair<int, const char*>>);

    ValuePair vp4{3.14f, "text"s};
    static_assert(is_same_v<decltype(vp4), ValuePair<float, string>>);

    const char* txt1 = "txt1";
    const char* txt2 = "txt2";

    ValuePair vp5{txt1, txt2};

    ValuePair vp6{4};
}

TEST_CASE("locking with lock_guard")
{
    std::timed_mutex mtx_;

    std::lock_guard lk{mtx_};
}

namespace Optimized
{
    template <typename T1, typename T2>
    struct ValuePair
    {
        T1 fst;
        T2 snd;

        template <typename Arg1, typename Arg2>
        ValuePair(Arg1&& arg1, Arg2&& arg2) : fst{std::forward<Arg1>(arg1)}, snd{std::forward<Arg2>(arg2)}
        {}
    };

    template <typename T1, typename T2>
    ValuePair(T1, T2) -> ValuePair<T1, T2>;
}

TEST_CASE("CTAD - for optimized case")
{
    Optimized::ValuePair vp1{1, 3.14};

    const string text = "text";
    Optimized::ValuePair vp2{text, "c-string"};
}

template <typename T1, typename T2>
struct Aggregate
{
    T1 a;
    T2 b;
};

template <typename T1, typename T2>
Aggregate(T1, T2) -> Aggregate<T1, T2>;

TEST_CASE("Aggregate + CTAD")
{
    Aggregate agg1{1, 3.14};
}

TEST_CASE("CTAD in std lib")
{
    SECTION("pair")
    {
        pair p1{1, 3.14};

        pair p2{foo, "text"};   
    }

    SECTION("tuple")
    {
        tuple tp1{1, 3.14, "text", "text"s};
    }

    SECTION("optional")
    {
        optional opt1 = 4; // optional<int>
        optional opt2(4); // optional<int>

        optional<int> opt3;
    }

    SECTION("smart pointers")
    {
        auto uptr = make_unique<int>(5);

        shared_ptr sptr = move(uptr); // shared_ptr<int>

        weak_ptr wptr = sptr; // weak_ptr<int>
    }

    SECTION("function")
    {
        function f = foo; // function<void(int)>
    }

    SECTION("containers")
    {
        vector vec{1, 2, 3, 4};
        vector data = {1, 2, 3};

        array small_buffer = {1, 2, 3, 4, 5, 6, 6, 7};
    }
}

