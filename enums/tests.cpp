#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <cstddef>

#include "catch.hpp"

using namespace std;

enum class Coffee : uint8_t { espresso, cappucino, latte };

TEST_CASE("test")
{
    Coffee c1 = Coffee::espresso;

    // Coffee c2 = 1; // ill-formed
    
    underlying_type_t<Coffee> x = 2;
    Coffee c3{x}; // allowed in C++17
}

enum class Index : size_t {};

constexpr size_t to_size_t(Index i)
{
    return static_cast<size_t>(i);
}

struct Array
{
    vector<int> items;

    int& operator[](Index index)
    {
        return items[to_size_t(index)];
    }
};

TEST_CASE("using Index")
{
    Array arr{ { 1, 2, 3} };
    REQUIRE(arr[Index{0}] == 1);
}

TEST_CASE("std::byte")
{
    char legacy_byte = 13;
    ++legacy_byte;

    cout << legacy_byte << "\n";

    std::byte b1{13};
    b1 = ~(b1 << 2);

    cout << std::to_integer<int>(b1) << "\n";
}