#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T>
string convert_to_string(T value)
{
    if constexpr(is_arithmetic_v<T>)
    {
        return to_string(value);
    }
    else if constexpr(is_same_v<T, string>)
    {
        return value;
    }
    else
    {
        return string(value);
    }    
}

TEST_CASE("constexpr if")
{
    SECTION("convert to string")
    {
        SECTION("from number")
        {
            string txt = convert_to_string(4);
            REQUIRE(txt == "4"s);
        }

        SECTION("string")
        {
            string txt = convert_to_string("txt"s);
            REQUIRE(txt == "txt"s);
        }

        SECTION("const char*")
        {
            string txt = convert_to_string("txt");
            REQUIRE(txt == "txt"s);
        }
    }
}

template <typename T, size_t N>
void process_array(T (&arr)[N])
{
    if constexpr(N <= 255)
    {
        cout << "Processing small table\n";
    }
    else
    {
        cout << "Processing large table\n";
    }
}

TEST_CASE("processing arrays")
{
    int tab1[1024];
    process_array(tab1);

    int tab2[10];
    process_array(tab2);
}

namespace BeforeCpp17
{
    void print()
    {
        cout << "\n";
    }

    template <typename Head, typename... Tail>
    void print(const Head& head, const Tail&... tail)
    {
        cout << head << " ";
        print(tail...);
    }
}

namespace Cpp17
{
    template <typename Head, typename... Tail>
    void print(const Head& head, const Tail&... tail)
    {
        cout << head << " ";

        if constexpr(sizeof...(tail) > 0)
        {
            print(tail...);
        }
        else
        {
            cout << "\n";
        }
    }
}

void foo()
{
    if constexpr(sizeof(int) <= 4)
    {
        cout << "sizeof(int) <= 4\n";
        static_assert(sizeof(int) <= 4);
    }
    else
    {
        cout << "sizeof(int) > 4\n";
        //static_assert(sizeof(int) > 4); // always error
    }    
}


TEST_CASE("variadic templates")
{
    BeforeCpp17::print(1, 3.14, "text"s);

    Cpp17::print(1, 3.14, "text"s);

    cout << "sizeof(int) : " << sizeof(int) << endl;
    foo();
}