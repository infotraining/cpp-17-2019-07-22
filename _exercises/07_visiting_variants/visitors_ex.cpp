#include <iostream>
#include <string>
#include <variant>
#include <vector>

#include "catch.hpp"

using namespace std;

struct Circle
{
    int radius;
};

struct Rectangle
{
    int width, height;
};

struct Square
{
    int size;
};

TEST_CASE("visit a shape variant and calculate area")
{
    using Shape = variant<Circle, Rectangle, Square>;

    vector<Shape> shapes = {Circle{1}, Square{10}, Rectangle{10, 1}};

    double total_area{};

    // TODO

    REQUIRE(total_area == Approx(113.14));
}