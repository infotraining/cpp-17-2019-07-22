#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    template <typename Head>
    auto sum(Head head)
    {
        return head;
    }

    template <typename Head, typename... Tail>
    auto sum(Head head, Tail... tail)
    {
        return head + sum(tail...);
    }
}

template <typename... Args>
auto sum(Args... args)
{
    return (... + args); // left unary fold
}

template <typename... Args>
auto sum_right(Args... args)
{
    return (args + ...); // right unary fold
}

template <typename... Args>
void print(const Args&... args)
{
    bool is_first = true;

    auto with_space = [&is_first](const auto& arg) { 
        if (!is_first)
            cout << " ";
        is_first = false;
        return arg;
    };

    (cout << ... << with_space(args)) << "\n"; // binary left-fold expression
}

template <typename... Args>
void print_all(const Args&... args)
{
    ((cout << args << " "), ...); // fold expression for , operator
    cout << "\n";
}

TEST_CASE("fold expressions")
{
    auto result = sum(1, 2, 3.5, 4);
    REQUIRE(result == Approx(10.5));

    print(1, 3.14, "text"s);
    print_all(1, 3.14, "text"s);
}