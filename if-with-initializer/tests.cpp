#include <algorithm>
#include <iostream>
#include <mutex>
#include <numeric>
#include <queue>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("if with initilizer")
{
    vector vec = {1, 2, 3, 665, 4};

    if (auto pos = find(begin(vec), end(vec), 665); pos != end(vec))
    {
        cout << "Item " << *pos << " has been found...\n";
    }
    else if (auto magic_value = 665; magic_value % 2 == 0)
    {
        cout << "Item has not been found...\n";
        assert(pos == end(vec));
    }
    else 
    {
        assert(pos == end(vec));
        assert(magic_value % 2 != 0);
    }
}

TEST_CASE("use case with mutex")
{
    queue<int> q;
    mutex q_mutex;

    SECTION("Before C++17")
    {
        {
            lock_guard<mutex> lk {q_mutex}; // q_mutex.lock()

            if (!q.empty())
            {
                auto value = q.front();
                q.pop();
            }
        } // q_mutex.unlock();
    }

    SECTION("Since C++17")
    {
        if (lock_guard lk{q_mutex}; !q.empty()) // BUG
        {
            auto value = q.front();
            q.pop();
        }
    }
}