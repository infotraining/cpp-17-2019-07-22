#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <variant>

#include "catch.hpp"

using namespace std;

TEST_CASE("variant")
{
    variant<int, double, string> var;

    REQUIRE(holds_alternative<int>(var));
    REQUIRE(get<int>(var) == 0);

    var = 3.14;
    var = "text"s;

    REQUIRE(var.index() == 2);
    REQUIRE_THROWS_AS(get<int>(var), bad_variant_access);

    REQUIRE(*get_if<string>(&var) == "text"s);
    REQUIRE(get_if<int>(&var) == nullptr);
}

struct Printer
{
    void operator()(int x) const
    {
        cout << "int: " << x << "\n";
    }

    void operator()(double x) const
    {
        cout << "double: " << x << "\n";
    }

    void operator()(const string& x) const
    {
        cout << "string: " << x << "\n";
    }
};

template <typename... Ts>
struct overload : Ts...
{
    using Ts::operator()...;
};

template <typename... Ts>
overload(Ts...) -> overload<Ts...>;

TEST_CASE("visiting variants")
{
    variant<int, double, string> var = "text"s;

    Printer prn;
    visit(prn, var);

    var = 3.14;
    visit(Printer{}, var);

    auto saver = overload {
        [](int a) { cout << "save int: " << a << "\n";},
        [](double b) { cout << "save double: " << b << "\n";},
        [](const string& s) { cout << "save string: " << s << "\n"; }        
    };

    visit(saver, var);
}

struct ErrorCode
{
    string msg;
    errc err_code;
};

[[nodiscard]] variant<string, ErrorCode> load_from_file(const string& file_name)
{
    if (file_name == "_")
    {
        return ErrorCode{"bad file name", errc::bad_file_descriptor};
    }

    return "text"s;
}

TEST_CASE("handling the result")
{
    visit(overload{
        [](const string& s) { cout << "Printing: " << s << "\n"; },
        [](const ErrorCode& ec) { cout << "Error: " << ec.msg << " - " << static_cast<int>(ec.err_code) << "\n"; }
    }, load_from_file("_"));
}