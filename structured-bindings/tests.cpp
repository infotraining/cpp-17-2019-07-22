#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <tuple>
#include <vector>
#include <map>
#include <list>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        vector<int>::const_iterator min, max;
        tie(min, max) = minmax_element(data.begin(), data.end());

        double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

        return make_tuple(*min, *max, avg);
    }
}

namespace Cpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        auto [min_it, max_it] = minmax_element(data.begin(), data.end());

        double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

        return tuple(*min_it, *max_it, avg);
    }
}

TEST_CASE("Before C++17")
{
    vector<int> data = {4, 42, 665, 1, 123, 13};

    int min, max;
    double avg;

    tie(min, max, avg) = BeforeCpp17::calc_stats(data);

    REQUIRE(min == 1);
    REQUIRE(max == 665);
    REQUIRE(avg == Approx(141.333));
}

TEST_CASE("Since C++17 - structured bindings")
{
    vector<int> data = {4, 42, 665, 1, 123, 13};

    auto [min, max, avg] = Cpp17::calc_stats(data);

    REQUIRE(min == 1);
    REQUIRE(max == 665);
    REQUIRE(avg == Approx(141.333));
}

auto get_coord() -> int (&)[3]
{
    static int pos[3] = {1, 2, 3};

    return pos;
}

std::array<int, 3> get_velocity()
{
    return std::array {1, 2, 3};
}

struct Error
{
    int error_code;
    string msg;
};

struct ErrorSpecs : Error
{
};

Error open_file()
{
    return {13, "file not found"};
}

void print_err(int err_c, const string& msg)
{
    cout << "Error code: " << err_c << " - " << msg << "\n";
}

TEST_CASE("stuctured bindings")
{
    SECTION("allow to decompose array")
    {
        auto [x, y, z] = get_coord();

        REQUIRE(x == 1);
        REQUIRE(y == 2);
        REQUIRE(z == 3);

        int pos[2] = {3, 7};

        auto [pos_x, pos_y] = pos;

        REQUIRE(pos_x == 3);
        REQUIRE(pos_y == 7);
    }

    SECTION("allow to decompose std::array")
    {
        auto [vx, vy, vz] = get_velocity();

        REQUIRE(vx == 1);
        REQUIRE(vy == 2);
        REQUIRE(vz == 3);
    }

    SECTION("allow to decompose std::tuple")
    {
        auto [x, name] = make_tuple(42, "text"s);

        REQUIRE(name == "text"s);
    }

    SECTION("allow to decompose std::tuple")
    {
        auto [x, name] = make_pair(42, "text"s);

        REQUIRE(name == "text"s);
    }

    SECTION("allow to decompose a struct")
    {
        auto [error_code, message] = open_file();
        print_err(error_code, message);

        REQUIRE(error_code == 13);
    }
}

TEST_CASE("how it works")
{
    tuple tpl{1, 3.14, "test"s};

    auto [x, pi, text] = tpl;

    x = 42;

    SECTION("is interpreted as")
    {
        auto unnamed_obj = tpl;

        auto& x = get<0>(tpl);
        auto& pi = get<1>(tpl);
        auto& text = get<2>(tpl);
    }
}

TEST_CASE("use auto& to modify l-value")
{
    tuple tpl{1, 3.14, "test"s};

    auto& [x, pi, text] = tpl;

    x = 42;

    REQUIRE(get<0>(tpl) == 42);
}

TEST_CASE("&, const & volatile, alignas")
{
    vector<int> data = {1, 2, 3};

    SECTION("const auto&")
    {
        auto f = data[0];

        auto [min, max, avg] = Cpp17::calc_stats(data);

        //++min; // ERROR
    }

    SECTION("const auto&")
    {
        const auto [min, max, avg] = Cpp17::calc_stats(data);
    }
}


struct Data
{
    int x;
    int& y;
};

TEST_CASE("beware - names are references")
{
    int value = 10;

    Data data{42, value};

    auto [xx, yy] = data;

    yy = 42;

    static_assert(is_same_v<int, decltype(xx)>);
    static_assert(is_same_v<int&, decltype(yy)>);

    REQUIRE(value == 42);
}


TEST_CASE("use cases")
{
    map<int, string> dict = { {1, "one"}, {2, "two"}, {3, "three"} };

    SECTION("iteration over map")
    {
        for(const auto& [key, value] : dict)
            cout << key << " - " << value << "\n";
    }

    SECTION("insert into associative container")
    {        
        if (const auto& [pos, ok] = dict.insert(pair(4, "four")); ok)
        {   
            const auto& [key, value] = *pos;
            cout << key << " - " << value << " was inserted to map...\n";
        }
    }

    SECTION("init for many vars")
    {
        auto [x, y, is_ok] = tuple(1, 3.14, true);

        list lst = { 42, 665, 13, 8 };

        for(auto&& [index, pos] = tuple(0, begin(lst)); pos != end(lst); ++index, ++pos)
        {
            cout << "item " << index << " - " << *pos << "\n";
        }
    }
}

enum Something { some, thing };

const map<int, string> something_desc = { {0, "some"}, {1, "thing"} };

// step 1 - describe the tuple-like size
template <>
struct std::tuple_size<Something> 
{
    static constexpr size_t value = 2;
};

// step 2 - describe types of items
template <>
struct std::tuple_element<0, Something>
{
    using type = int;
};

template <>
struct std::tuple_element<1, Something> : std::common_type<string>
{};

// step 3 - how to get values from an object
template <size_t N>
decltype(auto) get(const Something& s);

template <>
decltype(auto) get<0>(const Something& s)
{
    return static_cast<int>(s);
}

template <>
decltype(auto) get<1>(const Something& s)
{    
    return something_desc.at(static_cast<int>(s));
}

TEST_CASE("tuple protocol - structured bindings")
{
    Something sth = some;

    const auto [value, description] = sth; // structured binding for enum using tuple protocol

    REQUIRE(value == 0);
    REQUIRE(description == "some"s);
}
