#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct SimpleAggregate
{
    int a;
    double b;
    int tab[3];
    string name;

    void print() const
    {
        cout << "SimpleAggregate{ " << a << ", " << b << ", [ ";
        for(const auto& item : tab)
            cout << item << " ";
        cout << "], " << name << "}\n";
    }
};

TEST_CASE("simple aggregate")
{   
    SimpleAggregate agg1{1, 3.14, {1, 2, 3}, "aggreagate"};
    agg1.print();

    SimpleAggregate agg2{42};
    agg2.print();

    SimpleAggregate agg3{};
    agg3.print();
}

TEST_CASE("arrays are aggregates")
{
    int tab1[10] = {1, 2, 3};
    int tab2[10] = {};
}

struct ComplexAggregate : SimpleAggregate, string
{
    vector<int> vec;
};

TEST_CASE("ComplexAggregate")
{
    ComplexAggregate agg1{ {1, 42, {9, 5, 2}, "simple aggregate"}, {"text"}, {1, 2, 3, 4, 5} };

    agg1.print();
    REQUIRE(agg1.size() == 4);
    REQUIRE(agg1.vec.size() == 5);

    static_assert(is_aggregate_v<ComplexAggregate>);
}

struct WTF
{
    int value;

    WTF() = delete;
};

TEST_CASE("WTF")
{
    WTF wtf{}; // aggregate initialization

    REQUIRE(wtf.value == 0);
}