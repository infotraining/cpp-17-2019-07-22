#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <string_view>

#include "catch.hpp"

using namespace std;

template <typename Container>
void print(const Container& container, string_view prefix)
{
    cout << prefix << ": [";
    for(const auto& item : container)
    {
        cout << item << " ";        
    }
    cout << "]\n";
}

TEST_CASE("string_view")
{
    vector vec = {1, 2, 3, 4};

    print(vec, "vec");

    string desc = "vec";
    print(vec, desc);

    print(vec, "vec"s);

    print(vec, "vec"sv);
}

string_view get_prefix(string_view text)
{
    assert(text.size() >= 2);

    return string_view(text.data(), 2);
}

TEST_CASE("unsafe code")
{
    const char* text = "aabb";
    auto prefix = get_prefix(text);

    REQUIRE(prefix == "aa"sv);

    REQUIRE(get_prefix("aabb"s) == "aa"sv); // BUG - dangling pointer

    string_view unsafe = "aa"s; // BUG - dangling pointer

    string_view safe = "aa";
}

TEST_CASE("string_view")
{
    SECTION("construction")
    {
        string_view sv1 = "text";
        REQUIRE(sv1 == "text"sv);

        string str = "text string";
        string_view sv2 = str;        

        auto text = "aabbff";
        string_view sv3{text, 2};
    }

    SECTION("conversion to string")
    {
        string_view sv = "text";
        string str(sv); // explicit conversion using constructor        
    }
}