#include <algorithm>
#include <any>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <typeinfo>

#include "catch.hpp"

using namespace std;

TEST_CASE("any")
{
    any ao1 = 4;
    ao1 = 3.14;
    ao1 = vector {1, 2, 3};
    ao1 = "text"s;

    SECTION("any_cast - copy")
    {
        string str = any_cast<string>(ao1);
        REQUIRE(str == "text"s);

        REQUIRE_THROWS_AS(any_cast<int>(ao1), bad_any_cast);
    }

    SECTION("any_cast - pointer")
    {
        string* pstr = any_cast<string>(&ao1);
        REQUIRE(pstr != nullptr);

        int* pint = any_cast<int>(&ao1);
        REQUIRE(pint == nullptr);
    }

    REQUIRE(ao1.has_value());
    auto& type_identifier = ao1.type();

    cout << type_identifier.name() << endl;

    ao1.reset();
    REQUIRE_FALSE(ao1.has_value());
}