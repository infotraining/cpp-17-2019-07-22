#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <optional>
#include <complex>
#include <charconv>

#include "catch.hpp"

using namespace std;

TEST_CASE("optional")
{
    SECTION("construction without state")
    {
        optional<int> opt1;
        REQUIRE(opt1.has_value() == false);

        optional<int> opt2 = nullopt;
        REQUIRE(opt2.has_value() == false);
    }

    SECTION("construction with state")
    {
        optional<int> opt1{4};
        REQUIRE(opt1.has_value());

        optional opt2 = 4;
        REQUIRE(opt2.has_value());
    }

    SECTION("contuction in-place")
    {
        optional<complex<double>> opt_x{ in_place, 3.14, 6.28 };
    }

    SECTION("getting a value")
    {
        optional text = "optional"s;

        if (text)
        {
            cout << *text << "\n";
            REQUIRE(text->size() == 8);
        }
    }

    SECTION("value()")
    {
        optional n = 5;
        REQUIRE(n.value() == 5);
        REQUIRE(n.value_or(-1) == 5);

        n = nullopt;
        REQUIRE_THROWS_AS(n.value(), bad_optional_access);
        REQUIRE(n.value_or(-1) == -1);
    }

    SECTION("move semantics")
    {
        optional opt_text = "text";
        REQUIRE(opt_text.has_value());

        string target = std::move(*opt_text);
        REQUIRE(opt_text.has_value());
        opt_text.reset();
    }
}

optional<int> to_int(string_view str)
{
    int result{};

    auto start = str.data();
    auto end = str.data() + str.size();

    if (auto [pos, error_code] = from_chars(start, end, result); error_code != std::errc{} || pos != end)
    {
        return std::nullopt;
    }

    return result;
}

TEST_CASE("to_int")
{
    optional<int> n = to_int("665");

    REQUIRE(n.has_value());
    REQUIRE(*n == 665);

    n = to_int("4a");
    REQUIRE(n.has_value() == false);
}