#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("auto - bug fix")
{
    int a = 1;
    int b(2);
    int b{3};

    string s1 = "text";
    string s2("text");
    string s3{"text"};

    SECTION("auto")
    {
        auto a = 1; // int
        auto b(2);  // int
        auto c{3};  // int - since C++17
        //auto c4{1, 2, 3}; // since C++17 ill-formed
        auto d = {3}; // initializer_list<int>

        auto s1 = "text"s;
        auto s2("text"s);
        auto s3{"text"s};
    }
}

TEST_CASE("initializer list")
{
    auto il = { 1, 2, 3 };

    for(const auto& item : {1, 2, 3})
        cout << item << " - " << "\n";

    vector<int> vec{1, 2, 3};
    vec.insert(begin(vec), {4, 5, 6});
}